
// using avconv (ubuntu)
// g++ -DAVCONV -O2 -Wall -o clipator clipator.cpp `pkg-config --libs --cflags gtkmm-2.4 libvlc`

// using ffmpeg
// g++ -O2 -Wall -o clipator clipator.cpp `pkg-config --libs --cflags gtkmm-2.4 libvlc`

#include <gtkmm.h>
#include <gdk/gdkx.h>
#include <vlc/vlc.h>
#include <cassert>
#include <cstdlib>
#include <iostream>

void timeChangedCallback(const struct libvlc_event_t * ptrEvent, void * data);
void endReachedCallback(const struct libvlc_event_t * ptrEvent, void * data);

class ClipperWindow : public Gtk::Window {

  Gtk::VBox _vBox;

  Gtk::DrawingArea _drawingArea;

  Gtk::HScale _hScale;
  sigc::connection _hScaleConnect;

  Gtk::HBox _hBox;

  Gtk::Frame _filesFrame;
  Gtk::Table _filesGrid;
  Gtk::Button _filesOpenButton;
  Gtk::Label _filesLabel;
  Gtk::Button _filesExtractButton;
  Gtk::Entry _filesEntry;

  Gtk::Frame _playerFrame;
  Gtk::Table _playerGrid;
  Gtk::Button _playerPlayPauseButton;

  Gtk::Frame _startFrame;
  Gtk::Table _startGrid;
  Gtk::SpinButton _startSpin;
  Gtk::Button _startSetButton;
  Gtk::Button _startGotoButton;

  Gtk::Frame _endFrame;
  Gtk::Table _endGrid;
  Gtk::SpinButton _endSpin;
  Gtk::Button _endSetButton;
  Gtk::Button _endGotoButton;

  libvlc_instance_t *_ptrVlcInst;
  libvlc_media_player_t *_ptrVlcMediaPlayer;
  Glib::ustring _inputPath;
  
  void openInputPath(Glib::ustring inputPath) {
    if (not inputPath.empty()) {
      _inputPath = inputPath;
    
      // load new media
      libvlc_media_t * ptrNewMedia = libvlc_media_new_path(_ptrVlcInst, inputPath.c_str());
      if (ptrNewMedia) {      
	libvlc_media_player_set_media(_ptrVlcMediaPlayer, ptrNewMedia);
      
	// tricky crap
	libvlc_media_player_play(_ptrVlcMediaPlayer);
	sleep(1);
	if (libvlc_media_player_will_play(_ptrVlcMediaPlayer)) {
	  libvlc_media_player_pause(_ptrVlcMediaPlayer);
	  libvlc_media_player_set_time(_ptrVlcMediaPlayer, 0);
      
	  // update _filesLabel, _playerPlayPauseButton
	  int ipos = inputPath.find_last_of('/') + 1;
	  Glib::ustring filename = inputPath.substr(ipos, Glib::ustring::npos);
	  _filesLabel.set_label(filename);
	  _playerPlayPauseButton.set_label("Play");

	  // update _hScale, _startSpin, _endSpin
	  float nbSeconds = libvlc_media_player_get_length(_ptrVlcMediaPlayer) * 0.001;
	  _hScale.set_range(0, nbSeconds);
	  _hScale.set_value(0);
	  _startSpin.set_range(0, nbSeconds);
	  _endSpin.set_range(0, nbSeconds);
	}
	// invalid media
	else {
	  _inputPath.clear();
	  _filesLabel.set_label("");
	  _playerPlayPauseButton.set_label("Play");
	  _hScale.set_range(0, 1);
	  _hScale.set_value(0);
	  _startSpin.set_range(0, 1);
	  _startSpin.set_value(0);
	  _endSpin.set_range(0, 1);
	  _endSpin.set_value(0);
	}
      }
    }
  }

  bool isValidMedia() {

    // no valid input path
    if (_inputPath.empty()) return false;
    
    // no media
    if (not libvlc_media_player_get_media(_ptrVlcMediaPlayer)) return false;
    
    // reload media if ended
    if (libvlc_media_player_get_state(_ptrVlcMediaPlayer) == libvlc_Ended)
      openInputPath(_inputPath);

    // invalid media
    if (not libvlc_media_player_will_play(_ptrVlcMediaPlayer)) return false;

    // else media ok
    return true;
  }

  void gotoTime(float timeSec) {
    if (isValidMedia()) {
      _hScale.set_value(timeSec);
      libvlc_media_player_set_time(_ptrVlcMediaPlayer, timeSec * 1000);
    }
  }

  void onDrawingAreaRealize() {
    // attach media player to drawing area
    uint32_t id = GDK_WINDOW_XID(Glib::unwrap(_drawingArea.get_window()));
    libvlc_media_player_set_xwindow(_ptrVlcMediaPlayer, id);
  }
  
  void onFileOpenButtonClicked() {
    // launch dialog box
    Gtk::FileChooserDialog dialog(*this, "Open media...");
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
    int ret = dialog.run();

    // open selected file
    if (ret == Gtk::RESPONSE_OK) {
      openInputPath(dialog.get_filename());
    }
  }
  
  void onFileExtractButtonClicked() {
    if (_inputPath.empty()) {
      std::cerr << "error: no input media\n";
    }
    else {
      // find base path
      int ipos = _inputPath.find_last_of('/') + 1;
      Glib::ustring basePath = _inputPath.substr(0, ipos);

      // build output path
      Glib::ustring outputPath = basePath + _filesEntry.get_text();;

      // get start time and duration
      int startTimeSec = _startSpin.get_value();
      int endTimeSec = _endSpin.get_value();
      if (startTimeSec == endTimeSec) {
	std::cerr << "error: invalid start/end\n";
      }
      else {	
	int t0 = std::min(startTimeSec, endTimeSec);
	int t1 = std::max(startTimeSec, endTimeSec);
      
	// call ffmpeg cli to extract file
	std::ostringstream oss;
#ifdef AVCONV
	oss << "avconv -y -i \"" << _inputPath << "\" -ss " << t0 << " -t " << t1-t0 << " -vcodec copy -acodec copy \"" << outputPath << "\"";
#else
	oss << "ffmpeg -y -i \"" << _inputPath << "\" -ss " << t0 << " -to " << t1 << " -vcodec copy -acodec copy \"" << outputPath << "\"";
#endif
	std::cout << oss.str() << std::endl;
	int ret = system(oss.str().c_str());
	if (ret != 0) std::cout << "error during extraction"  << std::endl;
	else std::cout << "extraction ok"  << std::endl;
      }
    }
  }

  void onPlayerPlayPauseButtonClicked() {
    if (isValidMedia()) {
      // switch to pause
      if (libvlc_media_player_is_playing(_ptrVlcMediaPlayer)) {
	libvlc_media_player_pause(_ptrVlcMediaPlayer);
	_playerPlayPauseButton.set_label("Play");
      }
      // switch to play
      else {
	libvlc_media_player_play(_ptrVlcMediaPlayer);
	libvlc_media_player_pause(_ptrVlcMediaPlayer);
	_playerPlayPauseButton.set_label("Pause");
      }
    }
  }

  void onHScaleValueChanged() {
      float timeSec = _hScale.get_value();
      gotoTime(timeSec);
  }

  void onStartSetButton() {
    float timeSec = _hScale.get_value();
    _startSpin.set_value(timeSec);
  }
  
  void onStartGotoButton() {
    float timeSec = _startSpin.get_value();
    gotoTime(timeSec);
  }
  
  void onEndSetButton() {
    float timeSec = _hScale.get_value();
    _endSpin.set_value(timeSec);
  }
  
  void onEndGotoButton() {
    float timeSec = _endSpin.get_value();
    gotoTime(timeSec);
  }

public:
  
  ClipperWindow() {

    // init window
    set_title("Clipper 1.0");
    set_default_size(640, 500);
    
    // init libvlc
    _ptrVlcInst = libvlc_new(0, NULL);
    _ptrVlcMediaPlayer = libvlc_media_player_new(_ptrVlcInst);
    libvlc_event_manager_t * ptrEventManager = libvlc_media_player_event_manager(_ptrVlcMediaPlayer);
    libvlc_event_attach(ptrEventManager, libvlc_MediaPlayerTimeChanged, (libvlc_callback_t)timeChangedCallback, this);
    libvlc_event_attach(ptrEventManager, libvlc_MediaPlayerEndReached, (libvlc_callback_t)endReachedCallback, this);

    // init vbox
    add(_vBox);

    // init drawing area
    _vBox.pack_start(_drawingArea, true, true);
    _drawingArea.signal_realize().connect(sigc::mem_fun(*this, &ClipperWindow::onDrawingAreaRealize));
    
    // init hscale
    _vBox.pack_start(_hScale, false, false);
    _hScale.set_range(0, 1);
    _hScale.set_value(0);
    _hScale.set_increments(0.5, 10);
    _hScale.set_digits(1);
    _hScaleConnect = _hScale.signal_value_changed().connect(sigc::mem_fun(*this, &ClipperWindow::onHScaleValueChanged));	

    // init hbox
    _vBox.pack_start(_hBox, false, false);
    _hBox.pack_start(_filesFrame, false, false);
    _hBox.pack_start(_playerFrame, false, false);
    _hBox.pack_start(_startFrame, false, false);
    _hBox.pack_start(_endFrame, false, false);
    _hBox.set_spacing(8);

    // init files frame
    _filesFrame.add(_filesGrid);
    _filesFrame.set_label("Files");
    _filesGrid.resize(2, 2);
    _filesGrid.attach(_filesOpenButton, 0, 1, 0, 1);
    _filesGrid.attach(_filesLabel, 1, 2, 0, 1);
    _filesGrid.attach(_filesExtractButton, 0, 1, 1, 2);
    _filesGrid.attach(_filesEntry, 1, 2, 1, 2);
    _filesOpenButton.set_label("Open");
    _filesOpenButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onFileOpenButtonClicked));
    _filesLabel.set_label("");
    _filesExtractButton.set_label("Extract");
    _filesExtractButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onFileExtractButtonClicked));
    _filesEntry.set_text("clip.out");
    _filesEntry.set_width_chars(20);
    
    // init player frame
    _playerFrame.add(_playerGrid);
    _playerFrame.set_label("Player");
    _playerGrid.resize(1, 1);
    _playerGrid.attach(_playerPlayPauseButton, 0, 1, 0, 1);
    _playerPlayPauseButton.set_label("Play");
    _playerPlayPauseButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onPlayerPlayPauseButtonClicked));

    // init start frame
    _startFrame.add(_startGrid);
    _startFrame.set_label("Start");
    _startGrid.resize(2, 2);
    _startGrid.attach(_startSpin, 0, 2, 0, 1);
    _startGrid.attach(_startSetButton, 0, 1, 1, 2);
    _startGrid.attach(_startGotoButton, 1, 2, 1, 2);
    _startSpin.set_range(0, 1);
    _startSpin.set_digits(0);
    _startSpin.set_increments(1, 10);
    _startSpin.set_value(0);
    _startSpin.set_width_chars(10);
    _startSpin.signal_value_changed().connect(sigc::mem_fun(*this, &ClipperWindow::onStartGotoButton));
    _startSetButton.set_label("Set");
    _startGotoButton.set_label("Goto");
    _startSetButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onStartSetButton));
    _startGotoButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onStartGotoButton));

    // init end frame
    _endFrame.add(_endGrid);
    _endFrame.set_label("End");
    _endGrid.resize(2, 2);
    _endGrid.attach(_endSpin, 0, 2, 0, 1);
    _endGrid.attach(_endSetButton, 0, 1, 1, 2);
    _endGrid.attach(_endGotoButton, 1, 2, 1, 2);
    _endSpin.set_range(0, 1);
    _endSpin.set_digits(0);
    _endSpin.set_increments(1, 10);
    _endSpin.set_value(0);
    _endSpin.set_width_chars(10);
    _endSpin.signal_value_changed().connect(sigc::mem_fun(*this, &ClipperWindow::onEndGotoButton));
    _endSetButton.set_label("Set");
    _endGotoButton.set_label("Goto");
    _endSetButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onEndSetButton));
    _endGotoButton.signal_clicked().connect(sigc::mem_fun(*this, &ClipperWindow::onEndGotoButton));

    // show window
    show_all();
  }
  
  ~ClipperWindow() {
    libvlc_media_player_release(_ptrVlcMediaPlayer);
    libvlc_release(_ptrVlcInst);
  }

  void timeChanged(float time) {
    _hScaleConnect.disconnect();
    _hScale.set_value(time);
    _hScaleConnect = _hScale.signal_value_changed().connect(sigc::mem_fun(*this, &ClipperWindow::onHScaleValueChanged));
  }

  void endReached() {
    _playerPlayPauseButton.set_label("Play");
  }

  void initInputPath(const char* inputPath) {
    openInputPath(inputPath);
  }
};

void timeChangedCallback(const struct libvlc_event_t * ptrEvent, void * data) {
  ClipperWindow * ptrWin = (ClipperWindow *)data;
  assert(ptrEvent->type == libvlc_MediaPlayerTimeChanged);
  float time = ptrEvent->u.media_player_time_changed.new_time * 0.001;
  ptrWin->timeChanged(time);
}

void endReachedCallback(const struct libvlc_event_t * ptrEvent, void * data) {
  ClipperWindow * ptrWin = (ClipperWindow *)data;
  assert(ptrEvent->type == libvlc_MediaPlayerEndReached);
  ptrWin->endReached();
}

int main (int argc, char ** argv) {
  Gtk::Main app(argc, argv);
  ClipperWindow win;
  if (argc > 1) win.initInputPath(argv[1]);
  app.run(win);
  return 0;
}

