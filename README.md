# clipator
[![Build status](https://gitlab.com/juliendehos/clipator/badges/master/build.svg)](https://gitlab.com/juliendehos/clipator/pipelines) 

read a video and extract sequences

![](clipator.png)

## Build
install gtkmm, libvlc, ffmpeg (or avconv)

### build using ffmpeg:

```
g++ -O2 clipator.cpp `pkg-config --libs --cflags gtkmm-2.4 libvlc`
```

### build using avconf:

```
g++ -DAVCONV -O2 clipator.cpp `pkg-config --libs --cflags gtkmm-2.4 libvlc`
```

